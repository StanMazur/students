﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Students.BLL.DTO;
using Students.BLL.DTO.Requests;
using Students.BLL.Interfaces;

namespace Students.Controllers
{
    public class StudentsController : Controller
    {
        private readonly IStudentsService Context;

        public StudentsController(IStudentsService service)
                 {
            Context = service;
        }

        [HttpGet("Students/All")]
        [Authorize]
        public BaseResponse All()
        {
            return Context.GetAll();
        }

        [HttpGet("Students/Get")]
        [Authorize]
        public async Task<BaseResponse> Get([FromQuery] Guid id)
        {
            return await Context.GetAsync(id);
        }

        [HttpPost("Students/Crete")]
        [Authorize]
        public async Task<BaseResponse> Create([FromBody] StudentDTO request)
        {
            return await Context.CreateAsync(request);
        }
        
        [HttpPatch("Students/Update")]
        [Authorize]
        public async Task<BaseResponse> Update([FromBody] StudentDTO request)
        {
            return await Context.UpdateAsync(request);
        }

        [HttpDelete("Students/Delete")]
        [Authorize]
        public async Task<BaseResponse> Delete([FromQuery] Guid id)
        {
            return await Context.DeleteAsync(id);
        }

        [HttpPost("Students/GetFiltered")]
        [Authorize]
        public BaseResponse GetFiltered([FromBody] FilteredStudentRequest request)
        {
            return Context.GetFilteredStudents(request);
        }
    }
}