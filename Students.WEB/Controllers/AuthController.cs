﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using Students.Auth;
using Students.BLL.DTO;
using Students.BLL.Interfaces;
using Students.DAL.Entities;
using Students.Models;

namespace Students.Controllers
{
    public class AuthController : Controller
    {
        private IUserService Context;

        public AuthController(IUserService context)
        {
            Context = context;
        }
        
        [HttpPost("Auth/Token")]
        public IActionResult Token([FromBody]UserViewModel request)
        {
            var identity = GetIdentity(request.Login, request.Password);
            if (identity == null)
            {
                return BadRequest(new { errorText = "Invalid username or password." });
            }
 
            var now = DateTime.UtcNow;
            // create JWT - token
            var jwt = new JwtSecurityToken(
                    issuer: AuthOptions.ISSUER,
                    audience: AuthOptions.AUDIENCE,
                    notBefore: now,
                    claims: identity.Claims,
                    expires: now.Add(TimeSpan.FromMinutes(AuthOptions.LIFETIME)),
                    signingCredentials: new SigningCredentials(AuthOptions.GetSymmetricSecurityKey(), SecurityAlgorithms.HmacSha256));
            var encodedJwt = new JwtSecurityTokenHandler().WriteToken(jwt);
 
            var response = new
            {
                access_token = encodedJwt,
                username = identity.Name
            };
 
            return Json(response);
        }
 
        private ClaimsIdentity GetIdentity(string username, string password)
        {
            var mapper = new MapperConfiguration(cfg => cfg.CreateMap<User, UserViewModel>())
                .CreateMapper();
            var users = mapper.Map<IEnumerable<User>, IEnumerable<UserViewModel>>(Context.GetAll().Users);
            
            var person = users.FirstOrDefault(x => x.Login == username && x.Password == password);
            if (person == null) return null; // user not found
            var claims = new List<Claim>
            {
                new Claim(ClaimsIdentity.DefaultNameClaimType, person.Login),
                new Claim(ClaimsIdentity.DefaultRoleClaimType, person.Role)
            };
            var claimsIdentity =
                new ClaimsIdentity(claims, "Token", ClaimsIdentity.DefaultNameClaimType,
                    ClaimsIdentity.DefaultRoleClaimType);
            return claimsIdentity;
        }
    }
}