﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Students.BLL.DTO;
using Students.BLL.DTO.Requests;
using Students.BLL.Interfaces;

namespace Students.Controllers
{
    public class GroupController : Controller
    {
        private readonly IGroupService Context;

        public GroupController(IGroupService service)
        {
            Context = service;
        }
        
        [HttpGet("Group/All")]
        [Authorize]
        public BaseResponse All()
        {
            return Context.GetAll();
        }

        [HttpGet("Group/Get")]
        [Authorize]
        public async Task<BaseResponse> Get(Guid id)
        {
            return await Context.GetAsync(id);
        }

        [HttpPost("Group/Create")]
        [Authorize]
        public async Task<BaseResponse> Create([FromBody]GroupDTO group)
        {
            return await Context.CreateAsync(@group);
        }

        [HttpPatch("Group/Update")]
        [Authorize]
        public async Task<BaseResponse> Update([FromBody]GroupDTO group)
        {
            return await Context.UpdateAsync(@group);
        }

        [HttpDelete("Group/Delete")]
        [Authorize]
        public async Task<BaseResponse> Delete([FromQuery]Guid id)
        {
            return await Context.DeleteAsync(id);
        }

        [HttpPatch("Group/AddStudents")]
        [Authorize]
        public async Task<BaseResponse> AddStudents([FromBody]AddStudentsRequest request)
        {
            return await Context.AddStudentsAsync(request);
        }

        [HttpPost("Group/GetFiltered")]
        [Authorize]
        public BaseResponse GetFiltered([FromBody]FilteredGroupsRequest request)
        {
            return Context.GetFilteredGroup(request);
        }
    }
}