﻿using System.Text;
using Microsoft.IdentityModel.Tokens;

namespace Students.Auth
{
    public class AuthOptions
    {
        public const string ISSUER = "ServerApp"; // token issuer
        public const string AUDIENCE = "StudentsAppClient"; // token client
        const string KEY = "mysupersecret_secretkey!123";   // secret key
        public const int LIFETIME = 10; // token life time in minutes
        public static SymmetricSecurityKey GetSymmetricSecurityKey()
        {
            return new SymmetricSecurityKey(Encoding.ASCII.GetBytes(KEY));
        }
    }
}