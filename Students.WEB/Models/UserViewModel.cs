﻿using Microsoft.EntityFrameworkCore.Metadata.Internal;

namespace Students.Models
{
    //user of an application
    public class UserViewModel
    {
        public string Login { get; set; }
        public string Password { get; set; }
        public string Role { get; set; }
    }
}