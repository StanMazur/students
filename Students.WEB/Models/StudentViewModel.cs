﻿using System;

namespace Students.Models
{
    public class Student
    {
        public Guid Id { get; set; }
        public string Sex { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string SecondName { get; set; }
        public string UniqIdent { get; set; }
        public string FullName { get; set; }
        public string Groups { get; set; }
    }
}