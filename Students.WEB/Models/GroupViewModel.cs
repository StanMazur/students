﻿using System;

namespace Students.Models
{
    public class Group
    {
        public Guid id { get; set; }
        public string Name { get; set; }
        public int Count { get; set; }
    }
}