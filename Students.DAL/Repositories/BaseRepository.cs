﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Students.BLL.RepositoryInterfaces;
using Students.DAL.Context;

namespace Students.DAL.Repositories
{
    public class BaseRepository<T> : IRepository<T> 
        where T : class
    {
        protected ApplicationContext db;
        public BaseRepository(ApplicationContext context)
        {
            db = context;
        }
        //not asynchronous because when i started developing this project
        //I did not know that .net Core 3.1 does not have
        //ToLisAsync() and other methods
        public IEnumerable<T> GetAll()
        {
            return db.Set<T>();
        }

        public async Task<T> GetAsync(Guid id)
        {
            return await db.Set<T>().FindAsync(id);
        }

        //custom search
        //not asynchronous because when i started developing this project
        //I did not know that .net Core 3.1 does not have
        //ToLisAsync() and other methods
        public IEnumerable<T> Find(Func<T, bool> predicate)
        {
            return db.Set<T>().Where(predicate).ToList();
        }

        public async Task CreateAsync(T item)
        {
            await db.Set<T>().AddAsync(item);
            await db.SaveChangesAsync();
        }

        public async Task UpdateAsync(T item)
        {
            db.Entry(item).State = EntityState.Modified;
            await db.SaveChangesAsync();
        }

        public async Task DeleteAsync(Guid id)
        {
            var item = db.Set<T>().Find(id);
            if (item == null) return;
            db.Set<T>().Remove(item);
            await db.SaveChangesAsync();
        }
    }
}