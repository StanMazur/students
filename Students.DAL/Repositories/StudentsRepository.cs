﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Students.BLL.RepositoryInterfaces;
using Students.DAL.Context;
using Students.DAL.Entities;

namespace Students.DAL.Repositories
{
    public class StudentsRepository : BaseRepository<Student>, IStudentsRepository
    {
        public StudentsRepository(ApplicationContext context) : base(context)
        {
        }

        //filtering and pagination
        public IEnumerable<Student> GetFilteredStudents(
            int offset, int limit, 
            string filterSex = null, 
            string filterName = null,
            string filterUniqIdent = null, 
            string filterGroupName = null)
        {
            var data = db.Students
                .Include(sg => sg.Groups)
                .ThenInclude(g => g.Group)
                .ToList();
            //filtering
            data = string.IsNullOrEmpty(filterName)
            ? data.Where(st => st.FullName.Equals(filterName)).ToList()
            : data;
            data = string.IsNullOrEmpty(filterSex)
                ? data.Where(st => st.Sex.Equals(filterSex)).ToList()
                : data;
            data = string.IsNullOrEmpty(filterUniqIdent)
                ? data.Where(st => st.UniqIdent.Equals(filterUniqIdent)).ToList()
                : data;
            data = string.IsNullOrEmpty(filterGroupName)
                ? data.Where(st => 
                    st.Groups.Count(sg => sg.Group.Name.Equals(filterGroupName)) != 0)
                    .ToList()
                : data;
            //pagination
            if (offset + limit >= data.Count) return data.GetRange(offset, limit);
            return offset > data.Count ? new List<Student>() : data.GetRange(offset, data.Count - offset);
        }
    }
}