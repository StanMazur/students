﻿using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Students.BLL.RepositoryInterfaces;
using Students.DAL.Context;
using Students.DAL.Entities;

namespace Students.DAL.Repositories
{
    public class UsersRepository : BaseRepository<User>, IUserRepository
    {
        public UsersRepository(ApplicationContext context) : base(context)
        {
        }

        public async Task<User> GetByNameAsync(string name)
        {
            return await db.Users.FirstAsync(us => us.Login.Equals(name));
        }
    }
}