﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Students.BLL.RepositoryInterfaces;
using Students.DAL.Context;
using Students.DAL.Entities;

namespace Students.DAL.Repositories
{
    public class GroupsRepository : BaseRepository<Group>, IGroupRepository
    {
        public GroupsRepository(ApplicationContext context) : base(context)
        {
        }

        //add students to a group, many-to-many relation
        public async Task AddStudentsAsync(Guid groupId, List<Guid> studentsIdents)
        {
            var group = await GetAsync(groupId);
            var students = db.Students
                .Where(st => studentsIdents.Contains(st.Id))
                .ToList();
            group.Students = students
                .Select(st =>
                    new StudentsGroups
                    {
                        Group = group,
                        Student = st
                    })
                .ToList();
            await db.SaveChangesAsync();
        }
        //remove students from a group, many-to-many relation
        public async Task RemoveStudentAsync(Guid groupId, Guid studentId)
        {
            var group = db.Groups.FirstOrDefault(gr => gr.Id == groupId);
            var student = db.Students.FirstOrDefault(st => st.Id == studentId);

            if ((group == null) || (student == null)) return;
            {
                var sg = group.Students.FirstOrDefault(st => st.StudentId == student.Id);
                group.Students.ToList().Remove(sg);
                await db.SaveChangesAsync();
            }
        }

        public IEnumerable<Group> GetFilteredGroups(
            int offset, int limit, 
            string filterGroupName = null)
        {
            //group filtering
            var res =  string.IsNullOrEmpty(filterGroupName) 
                ? new List<Group>()
                : Find(gr => gr.Name.Equals(filterGroupName)).ToList();
            
            //pagination
            if (offset + limit <= res.Count)
            {
                //normal
                res = res.GetRange(offset, limit);
            }
            else
            {
                if (offset >= res.Count)
                {
                    //start number of pagination more than group count
                    res = new List<Group>();
                }

                res = res.GetRange(offset, res.Count - offset);
            }

            return res;
        }
    }
}