﻿using Microsoft.EntityFrameworkCore;
using Students.DAL.Entities;

namespace Students.DAL.Context
{
    public class ApplicationContext : DbContext
    {
        public DbSet<Student> Students { get; set; }
        public DbSet<Group> Groups { get; set; }
        public DbSet<User> Users { get; set; }
        
        public ApplicationContext(DbContextOptions<ApplicationContext> options) : base(options)
        {
            Database.EnsureCreated();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //uniqueness check
            modelBuilder.Entity<Student>()
                .HasIndex(st => st.UniqIdent)
                .IsUnique();
            //many-to-many relation
            modelBuilder.Entity<StudentsGroups>()
                .HasKey(sg => new {sg.GroupId, StudentGuid = sg.StudentId});
            modelBuilder.Entity<StudentsGroups>()
                .HasOne(sg => sg.Group)
                .WithMany(gr => gr.Students)
                .HasForeignKey(sg => sg.GroupId);
            modelBuilder.Entity<StudentsGroups>()
                .HasOne(sg => sg.Student)
                .WithMany(st => st.Groups)
                .HasForeignKey(sg => sg.StudentId);
        }
    }
    
}