﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore.Internal;
using Students.DAL.Context;

namespace Students.DAL.Entities
{
    //test data creating
    public class SampleDataCreator
    {
        public static void Initialize(ApplicationContext context)
        {
            if (!context.Students.Any())
            {
                var students = new List<Student>{
                    new Student
                    {
                        Name = "Alex",
                        Sex = "male",
                        Surname = "Ivanov",
                        SecondName = "Petrovich"
                    },
                    new Student
                    {
                        Name = "Sergio",
                        Sex = "male",
                        Surname = "Petrov",
                        SecondName = "Petrovich"
                    }
                };
                var groups = new List<Group>
                {
                    new Group
                    {
                        Name = "Mathematica"
                    },
                    new Group
                    {
                        Name = "Physic"
                    }
                };
                groups.First().Students = new List<StudentsGroups>
                {
                    new StudentsGroups
                    {
                        Student = students.First(),
                        Group = groups.First()
                    },
                    new StudentsGroups
                    {
                        Student = students.Last(),
                        Group = groups.First()
                    }
                };
                groups.Last().Students = new List<StudentsGroups>
                {
                    new StudentsGroups
                    {
                        Student = students.Last(),
                        Group = groups.Last()
                    }
                };

                context.AddRange(students);
                context.AddRange(groups); 
                context.SaveChanges();
            }

            if (!context.Users.Any())
            {
                var users = new List<User>
                {
                    new User {Login="admin@gmail.com", Password="12345", Role = "admin" },
                    new User { Login="user@gmail.com", Password="54321", Role = "user" }
                };
                context.AddRange(users);
                context.SaveChanges();
            }
        }
    }
}