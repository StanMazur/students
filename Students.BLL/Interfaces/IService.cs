﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using Students.BLL.DTO;

namespace Students.BLL.Interfaces
{
    public interface IService<in T> 
        where T : class
    {
        public BaseResponse GetAll();
        public Task<BaseResponse> GetAsync(Guid id);
        public Task<BaseResponse> CreateAsync(T item);
        public Task<BaseResponse> UpdateAsync(T item);
        public Task<BaseResponse> DeleteAsync(Guid id);

    }
}