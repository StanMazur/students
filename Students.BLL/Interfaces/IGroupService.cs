﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Threading.Tasks;
using Students.BLL.DTO;
using Students.BLL.DTO.Requests;

namespace Students.BLL.Interfaces
{
    public interface IGroupService : IService<GroupDTO>
    {
        public Task<BaseResponse> AddStudentsAsync(AddStudentsRequest request);

        public BaseResponse GetFilteredGroup(FilteredGroupsRequest request);
    }
}