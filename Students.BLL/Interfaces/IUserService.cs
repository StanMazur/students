﻿using System.Threading.Tasks;
using Students.BLL.DTO;
using Students.DAL.Entities;

namespace Students.BLL.Interfaces
{
    public interface IUserService : IService<User>
    {
        public new UserResponse GetAll();
        public Task<BaseResponse> GetByNameAsync(string userName);
    }
}