﻿using System.Collections.Generic;
using Students.BLL.DTO;
using Students.BLL.DTO.Requests;

namespace Students.BLL.Interfaces
{
    public interface IStudentsService : IService<StudentDTO>
    {
        public BaseResponse GetFilteredStudents(FilteredStudentRequest request);
    }
}