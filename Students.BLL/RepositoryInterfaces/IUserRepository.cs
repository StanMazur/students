﻿using System.Threading.Tasks;
using Students.DAL.Entities;

namespace Students.BLL.RepositoryInterfaces
{
    public interface IUserRepository : IRepository<User>
    {
        public Task<User> GetByNameAsync(string name);
    }
}