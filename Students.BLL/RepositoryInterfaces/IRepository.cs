﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Students.BLL.RepositoryInterfaces
{
    public interface IRepository<T> 
        where T : class
    {
        IEnumerable<T> GetAll();
        Task<T> GetAsync(Guid id);
        public IEnumerable<T> Find(Func<T, Boolean> predicate);
        Task CreateAsync(T item);
        Task UpdateAsync(T item);
        Task DeleteAsync(Guid id);
        
    }
}