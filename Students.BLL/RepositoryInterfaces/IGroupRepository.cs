﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Students.DAL.Entities;

namespace Students.BLL.RepositoryInterfaces
{
    public interface IGroupRepository : IRepository<Group>
    {
        public Task AddStudentsAsync(Guid groupId, List<Guid> studentsIdents);
        public Task RemoveStudentAsync(Guid groupId, Guid studentId);

        public IEnumerable<Group> GetFilteredGroups(
            int offset, int limit,
            string filterGroupName = null);
    }
}