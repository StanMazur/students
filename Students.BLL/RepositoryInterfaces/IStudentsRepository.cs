﻿using System.Collections.Generic;
using Students.DAL.Entities;

namespace Students.BLL.RepositoryInterfaces
{
    public interface IStudentsRepository : IRepository<Student>
    {
        public IEnumerable<Student> GetFilteredStudents(
            int offset, int limit,
            string filterSex = null,
            string filterName = null,
            string filterUniqIdent = null,
            string filterGroupName = null
            );
    }
}