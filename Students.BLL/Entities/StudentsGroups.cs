﻿using System;

namespace Students.DAL.Entities
{
    public class StudentsGroups
    {
        public Student Student { get; set; }
        public Guid StudentId { get; set; }

        public Group Group { get; set; }
        public Guid GroupId { get; set; }
    }
}