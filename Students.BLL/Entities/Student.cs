﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;

namespace Students.DAL.Entities
{
    public class Student
    {
        private string fullName;
        
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }
        [Required]
        public string Sex { get; set; }
        [Required]
        [MaxLength(40)]
        public string Surname { get; set; }
        [Required]
        [MaxLength(40)]
        public string Name { get; set; }
        [MaxLength(60)]
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string SecondName { get; set; }
        [StringLength(16, MinimumLength = 6)]
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string UniqIdent { get; set; }
        
        public string FullName
        {
            get => $"{Surname} {Name} {SecondName}";
        }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public IEnumerable<StudentsGroups> Groups { get; set; }
    }
}