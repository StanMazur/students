﻿namespace Students.BLL.DTO
{
    public class Enums
    {
        public enum Status
        {
            OK = 0,
            API_REQUEST_ERROR = 100,
            API_GROUP_NOT_FOUND,
            API_STUDENT_NOT_FOUND,
            API_NOT_EXIST
        }
    }
}