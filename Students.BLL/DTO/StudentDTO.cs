﻿using System;
using System.Buffers;
using Newtonsoft.Json;

namespace Students.BLL.DTO
{
    public class StudentDTO
    {
        public Guid Id { get; set; }
        public string Sex { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string SecondName { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string UniqIdent { get; set; }
        public string FullName { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string Groups { get; set; }
    }
}