﻿using System;

namespace Students.BLL.DTO
{
    public class GroupDTO
    {
        public Guid id { get; set; }
        public string Name { get; set; }
        public int Count { get; set; }
    }
}