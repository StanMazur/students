﻿using System.Collections.Generic;

namespace Students.BLL.DTO
{
    public class GroupResponse : BaseResponse
    {
        public List<GroupDTO> FilteredGroups { get; set; }
        public int Count { get; set; }
    }
}