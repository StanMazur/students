﻿
using System.Collections.Generic;

namespace Students.BLL.DTO
{
    public class StudentResponse : BaseResponse
    {
        public List<StudentDTO> Students { get; set; }
        public int Count { get; set; }

    }
}