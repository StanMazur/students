﻿namespace Students.BLL.DTO
{
    public class BaseResponse
    {
        public int Status { get; set; }
        public string Error { get; set; }
    }
}