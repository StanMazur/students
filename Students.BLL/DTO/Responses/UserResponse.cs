﻿using System.Collections.Generic;
using Students.DAL.Entities;

namespace Students.BLL.DTO
{
    public class UserResponse : BaseResponse
    {
        public List<User> Users { get; set; }
    }
}