﻿using System;

namespace Students.BLL.DTO.Requests
{
    public class BaseRequest
    {
        public Guid Id { get; set; }
        public int Offset { get; set; }
        public int Limit { get; set; }
    }
}