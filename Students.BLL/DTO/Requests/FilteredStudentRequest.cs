﻿namespace Students.BLL.DTO.Requests
{
    public class FilteredStudentRequest : BaseRequest
    {

        public string FilterSex { get; set; }
        public string FilterName { get; set; }
        public string FilterUniqIdent { get; set; }
        public string FilteredGroupName { get; set; }
        
    }
}