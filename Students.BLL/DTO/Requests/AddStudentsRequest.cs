﻿using System;
using System.Collections.Generic;

namespace Students.BLL.DTO.Requests
{
    public class AddStudentsRequest : BaseRequest
    {
        public Guid groupId { get; set; }
        public List<Guid> studentsIds { get; set; }
    }
}