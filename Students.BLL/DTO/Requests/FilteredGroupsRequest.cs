﻿namespace Students.BLL.DTO.Requests
{
    public class FilteredGroupsRequest : BaseRequest
    {
        public string filterGroupName { get; set; }
    }
}