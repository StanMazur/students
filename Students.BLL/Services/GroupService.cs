﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Students.BLL.DTO;
using Students.BLL.DTO.Requests;
using Students.BLL.Infrastructure;
using Students.BLL.Interfaces;
using Students.BLL.RepositoryInterfaces;
using Students.DAL.Entities;

namespace Students.BLL.Services
{
    public class GroupService : IGroupService
    {
        private IGroupRepository Repository { get; set; }

        public GroupService(IGroupRepository rep)
        {
            Repository = rep;
        }
        public BaseResponse GetAll()
        {
            var mapper = new MapperConfiguration(cfg => cfg.CreateMap<Group, GroupDTO>())
                .CreateMapper();
            var res = mapper
                .Map<IEnumerable<Group>, IEnumerable<GroupDTO>>(Repository.GetAll())
                .ToList();
            return new GroupResponse
            {
                Status = (int)Enums.Status.OK,
                FilteredGroups = res,
                Count = res.Count
            };
                
        }

        public async Task<BaseResponse> GetAsync(Guid id)
        {
            if (id == Guid.Empty) return new BaseResponse
            {
                Status = (int)Enums.Status.API_REQUEST_ERROR,
                Error = $"{Enums.Status.API_REQUEST_ERROR} Invalid input data"
            };
            var mapper = new MapperConfiguration(cfg => cfg.CreateMap<Group, GroupDTO>())
                .CreateMapper();
            var res = mapper.Map<Group, GroupDTO>(await Repository.GetAsync(id));
            return new GroupResponse
            {
                Status = (int)Enums.Status.OK,
                FilteredGroups = new List<GroupDTO>{res}
            };
        }

        public async Task<BaseResponse> CreateAsync(GroupDTO item)
        {
            if (item == null) return new BaseResponse
            {
                Status = (int)Enums.Status.API_REQUEST_ERROR,
                Error = $"{Enums.Status.API_REQUEST_ERROR} Invalid input data"
            };
            var group = new Group
            {
                Name = item.Name
            };
            await Repository.CreateAsync(group);
            return new BaseResponse
            {
                Status = (int)Enums.Status.OK
            };
        }

        public async Task<BaseResponse> UpdateAsync(GroupDTO item)
        {
            if (item == null) return new BaseResponse
            {
                Status = (int)Enums.Status.API_REQUEST_ERROR,
                Error = $"{Enums.Status.API_REQUEST_ERROR} Invalid input data"
            };
            var mapper = new MapperConfiguration(
                cfg => cfg.CreateMap<GroupDTO, Group>())
                .CreateMapper();
            await Repository.UpdateAsync(mapper.Map<GroupDTO, Group>(item));
            return new BaseResponse
            {
                Status = (int)Enums.Status.OK
            };
        }

        public async Task<BaseResponse> DeleteAsync(Guid id)
        {
            if (id == Guid.Empty) return new BaseResponse
            {
                Status = (int)Enums.Status.API_REQUEST_ERROR,
                Error = $"{Enums.Status.API_REQUEST_ERROR} Invalid input data"
            };
            await Repository.DeleteAsync(id);
            return new BaseResponse
            {
                Status = (int)Enums.Status.OK
            };
        }

        public async Task<BaseResponse> AddStudentsAsync(AddStudentsRequest request)
        {
             if (request.groupId == Guid.Empty)return new BaseResponse
             {
                 Status = (int)Enums.Status.API_REQUEST_ERROR,
                 Error = $"{Enums.Status.API_REQUEST_ERROR} Invalid input data"
             };
             if ((request.studentsIds == null)&&(request.studentsIds.Count == 0)) return new BaseResponse
             {
                 Status = (int)Enums.Status.API_REQUEST_ERROR,
                 Error = $"{Enums.Status.API_REQUEST_ERROR} Invalid input data"
             };
             
             await Repository.AddStudentsAsync(request.groupId, request.studentsIds);
             
             return new BaseResponse
             {
                 Status = (int)Enums.Status.OK
             };
        }

        public BaseResponse GetFilteredGroup(FilteredGroupsRequest request)
        {
            if (request == null) return new BaseResponse
            {
                Status = (int)Enums.Status.API_REQUEST_ERROR,
                Error = $"{Enums.Status.API_REQUEST_ERROR} Invalid input data"
            };
            var groups = Repository
                .GetFilteredGroups(request.Offset, request.Limit, request.filterGroupName)
                .ToList();
            var mapper = new MapperConfiguration(cfg => 
                cfg.CreateMap<Group, GroupDTO>())
                .CreateMapper();
            var res = mapper.Map<IEnumerable<Group>, IEnumerable<GroupDTO>>(groups).ToList();
            foreach (var groupDto in res)
            {
                groupDto.Count = groups.Count(gr => gr.Id == groupDto.id);
            }
            return new GroupResponse
            {
                Status = (int)Enums.Status.OK,
                Count = groups.Count,
                FilteredGroups = res
            };
        }
    }
}