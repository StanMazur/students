﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Students.BLL.DTO;
using Students.BLL.Interfaces;
using Students.BLL.RepositoryInterfaces;
using Students.DAL.Entities;

namespace Students.BLL.Services
{
    public class UserService : IUserService
    {
        private IUserRepository Repository;

        public UserService(IUserRepository repository)
        {
            Repository = repository;
        }

        public UserResponse GetAll() => new UserResponse {Users = Repository.GetAll().ToList()};

        BaseResponse IService<User>.GetAll()
        {
            return GetAll();
        }

        public async Task<BaseResponse> GetAsync(Guid id) =>
            new UserResponse
            {
                Users = new List<User> {await Repository.GetAsync(id)}
            };

        public async Task<BaseResponse> GetByNameAsync(string userName) =>
            new UserResponse
            {
                Users = new List<User>
                {
                    await Repository.GetByNameAsync(userName)
                }
            };

        public async Task<BaseResponse> CreateAsync(User item)
        {
            await Repository.CreateAsync(item);
            return new UserResponse
            {
                Status = (int) Enums.Status.OK
            };
        }

        public async Task<BaseResponse> UpdateAsync(User item)
        {
            await Repository.UpdateAsync(item);
            return new BaseResponse
            {
                Status = (int) Enums.Status.OK
            };
        }

        public async Task<BaseResponse> DeleteAsync(Guid id)
        {
            await Repository.DeleteAsync(id);
            return new UserResponse
            {
                Status = (int) Enums.Status.OK
            };
        }
    }
}