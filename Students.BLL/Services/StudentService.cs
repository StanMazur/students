﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Students.BLL.DTO;
using Students.BLL.DTO.Requests;
using Students.BLL.Infrastructure;
using Students.BLL.Interfaces;
using Students.BLL.RepositoryInterfaces;
using Students.DAL.Entities;

namespace Students.BLL.Services
{
    public class StudentService: IStudentsService
    {
        private IStudentsRepository Repository { get; set; }

        public StudentService(IStudentsRepository rep)
        {
            Repository = rep;
        }
        
        public BaseResponse GetAll()
        {
            var mapper = new MapperConfiguration(
                cfg => cfg.CreateMap<Student, StudentDTO>())
                .CreateMapper();
            var res = mapper.Map<IEnumerable<Student>, List<StudentDTO>>(Repository.GetAll());
            return new StudentResponse
            {
                Status = (int)Enums.Status.OK,
                Students = res,
                Count = res.Count
            };
        }

        public async Task<BaseResponse> GetAsync(Guid id)
        {
            if (id == Guid.Empty) return new BaseResponse
            {
                Status = (int)Enums.Status.API_REQUEST_ERROR,
                Error = $"{Enums.Status.API_REQUEST_ERROR} Invalid input data"
            };
            var mapper = new MapperConfiguration(
                    cfg => cfg.CreateMap<Student, StudentDTO>())
                .CreateMapper();
            var res = mapper.Map<Student, StudentDTO>(await Repository.GetAsync(id));

            return new StudentResponse
            {
                Status = (int)Enums.Status.OK,
                Students = new List<StudentDTO>{res}
            };
                
        }

        public async Task<BaseResponse> CreateAsync(StudentDTO item)
        {
            var student = new Student
            {
                Name = item.Name,
                SecondName = item.SecondName,
                Surname = item.Surname,
                UniqIdent = item.UniqIdent,
                Sex = item.Sex
            };
            await Repository.CreateAsync(student);
            return new BaseResponse
            {
                Status = (int)Enums.Status.OK
            };
        }

        public async Task<BaseResponse> UpdateAsync(StudentDTO item)
        {
            if (item == null) return new BaseResponse
            {
                Status = (int)Enums.Status.API_REQUEST_ERROR,
                Error = $"{Enums.Status.API_REQUEST_ERROR} Invalid input data"
            };
            var mapper = new MapperConfiguration(
                cfg => cfg.CreateMap<StudentDTO, Student>())
                .CreateMapper();
            await Repository.UpdateAsync(mapper.Map<StudentDTO, Student>(item));
            return new BaseResponse
            {
                Status = (int)Enums.Status.OK
            };
        }

        public async Task<BaseResponse> DeleteAsync(Guid id)
        {
            if (id == Guid.Empty) return new BaseResponse
            {
                Status = (int)Enums.Status.API_REQUEST_ERROR,
                Error = $"{Enums.Status.API_REQUEST_ERROR} Invalid input data"
            };
            
            await Repository.DeleteAsync(id);
            return new BaseResponse
            {
                Status = (int)Enums.Status.OK
            };
        }

        public BaseResponse GetFilteredStudents(FilteredStudentRequest request)
        {
            if (request == null) return new BaseResponse
            {
                Status = (int)Enums.Status.API_REQUEST_ERROR,
                Error = $"{Enums.Status.API_REQUEST_ERROR} Invalid input data"
            };
            var mapper = new MapperConfiguration(cfg => cfg.CreateMap<Student, StudentDTO>())
                .CreateMapper();
            
            var students =  Repository.GetFilteredStudents(
                request.Offset, request.Limit,
                request.FilterSex,
                request.FilterName,
                request.FilterUniqIdent,
                request.FilteredGroupName)
                .ToList();
            var res = mapper.Map<IEnumerable<Student>, IEnumerable<StudentDTO>>(students).ToList();
            foreach (var studentDto in res)
            {
                studentDto.Groups = string.Join(", ", students
                    .Where(st => st.Id == studentDto.Id)
                    .Select(sg => sg.Groups));
            }

            return new StudentResponse
            {
                Status = (int)Enums.Status.OK,
                Students = res,
                Count = res.Count
            };
        }
    }
}